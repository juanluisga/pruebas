package personas;

//PersonaEventListener.java
import java.util.EventListener;

interface PersonaEventListener extends EventListener {
  void NombreCambiado (PersonaEventObject args);
  void EdadCambiado (PersonaEventObject args);
}