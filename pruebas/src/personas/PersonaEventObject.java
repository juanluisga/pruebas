package personas;

//PersonaEventObject.java
import java.util.EventObject;

public class PersonaEventObject extends EventObject {
 public Persona persona;

 public PersonaEventObject (Object source, Persona persona)
 {
     super (source);
     this.persona = persona;
 }

 public Persona getPersona ()
 {
     return persona;
 }

}