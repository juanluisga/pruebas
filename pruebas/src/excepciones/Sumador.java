package excepciones;

public class Sumador {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		float sum = 0f;
		
			for (int i = 0; i < args.length; i++){
				try{
					sum = sum + Float.parseFloat(args[i]);
				} catch (NumberFormatException ex){
					System.out.println("Error: " + args[i] + "Debe introducir sólo números como parámetros.");
					throw ex;
				} 
			}
		System.out.println("Suma total: " + sum);
	}

}
