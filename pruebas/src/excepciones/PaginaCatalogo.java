package excepciones;

import java.net.*;

public class PaginaCatalogo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

			PaginaInicial[] catalogo = new PaginaInicial[5];
			
			try{
				catalogo[0] = new PaginaInicial("Google", "http://www.google.es", "Buscador");
				catalogo[1] = new PaginaInicial("Javier Torres", "http://www.hoy.es", "periodico");
				catalogo[2] = new PaginaInicial("Ernesto Vacas", "http://www.twitter.com", "red social");
				catalogo[3] = new PaginaInicial("Francisco Toledo", "http://www.foromtb.com", "foro");
				catalogo[4] = new PaginaInicial("Arantxa Farnes", "://javahispano.org", "comunidad");
				
				for (int i = 0; i < catalogo.length; i++){
					System.out.println(	catalogo[i].propietario + " - " +
										catalogo[i].direccion  + " - " +
										catalogo[i].categoria);
				}
			} catch (MalformedURLException ex){
				System.out.println("Error: " + ex.getMessage());
			}
	}

}
