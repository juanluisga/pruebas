package threads;

// import javax.swing.JApplet;
import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.net.*;

public class CarruselDeEnlaces 	extends 	JApplet 
								implements	Runnable,
											ActionListener{
	
	String[] tituloPagina = new String[6];
	URL[] enlacePagina = new URL[6];
	Color darColor = new Color(255, 204, 158);
	int actual = 0;
	Thread ejecutor;
	
	public void init(){
		tituloPagina[0] = "Buscador";
		enlacePagina[0] = getURL("http://www.google.com");
		tituloPagina[1] = "Red Social";
		enlacePagina[1] = getURL("http://www.twitter.com");
		tituloPagina[2] = "Ciclismo";
		enlacePagina[2] = getURL("http://www.foromtb.com");
		tituloPagina[3] = "Noticias";
		enlacePagina[3] = getURL("http://www.eldiario.com");
		tituloPagina[4] = "Agregador";
		enlacePagina[4] = getURL("http://www.meneame.net");
		tituloPagina[5] = "Programación";
		enlacePagina[5] = getURL("http://www.javahispano.org");
		Button botonIr = new Button("Ir");
		botonIr.addActionListener(this);
		setLayout(new FlowLayout());
		add(botonIr);
	}
	
	private URL getURL(String direccion){
		URL paginaURL = null;
		
		try{
			paginaURL = new URL(getDocumentBase(), direccion);
		} catch(MalformedURLException ex){
			
		}
		return paginaURL;
	}
	
	public void paint(Graphics screen){
		Graphics2D screen2D = (Graphics2D)screen;
		screen2D.setColor(darColor);
		screen2D.fillRect(0, 0, getSize().width, getSize().height);
		screen2D.setColor(Color.BLACK);
		screen2D.drawString(tituloPagina[actual], 5, 60);
		screen2D.drawString("" + enlacePagina[actual], 5, 80);
		
	}
	
	public void start(){
		if (ejecutor == null){
			ejecutor = new Thread(this);
			ejecutor.start();
		}
	}
	
	public void run(){
		Thread esteThread = Thread.currentThread();
		while (ejecutor == esteThread){
			actual++;
			if (actual > 5) actual = 0;
			repaint();
			try {
				Thread.sleep(10000);
			} catch(InterruptedException ex){
				// nada
			}
		}
	}
	
	/*
	public void stop(){
		if (ejecutor != null) ejecutor = null;
	}
	*/
	
	public void actionPerformed(ActionEvent evento){
		if (ejecutor != null) ejecutor = null;
		AppletContext navegador = this.getAppletContext();
		if (enlacePagina[actual] != null)
			navegador.showDocument(enlacePagina[actual]);
	}

}
