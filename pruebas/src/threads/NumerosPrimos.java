package threads;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class NumerosPrimos	extends 	JFrame
							implements 	Runnable, 
										ActionListener {
	
	Thread ir;
	JLabel etiquetaCuantos = new JLabel("Cantidad: ");
	JTextField cuantos = new JTextField("250", 10);
	JButton mostrar = new JButton("Mostrar primos");
	JTextArea primos = new JTextArea(8, 40);
	
	public NumerosPrimos(){
		super("Calcular números primos");
		setSize(400, 300);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
		mostrar.addActionListener(this);
		
		JPanel topPanel = new JPanel();
		topPanel.add(etiquetaCuantos);
		topPanel.add(cuantos);
		topPanel.add(mostrar);
		add(topPanel, BorderLayout.NORTH);
		
		primos.setLineWrap(true);
		JScrollPane bottonPanel = new JScrollPane(primos);
		// bottonPanel.add(primos);
		add(bottonPanel, BorderLayout.CENTER);
		
		setVisible(true);
		
		
	}
	
	public void actionPerformed(ActionEvent evento){
		
		mostrar.setEnabled(false);
		if (ir == null){
			ir = new Thread(this);
			ir.start();
		}
		
	}
	
	public void run(){
		int cantidad = Integer.parseInt(cuantos.getText());
		int numPrimos = 0;
		// candidato: el número puede ser primo.
		int candidato = 2;
		primos.append("Primeros " + cantidad + " números primos: ");
		while (numPrimos < cantidad){
			if (esPrimo(candidato)){
				primos.append(candidato + " ");
				numPrimos++;
			}
			candidato++;
		}
	}
	
	public static boolean esPrimo(int numero){
		double raiz = Math.sqrt(numero);
		for (int i = 2; i <= raiz; i++){
			if (numero % i == 0) return false;
		}
		return true;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		NumerosPrimos numeros = new NumerosPrimos();
	}

}
