package threads;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.net.*;

public class CarruselGui	extends 	JFrame
							implements	Runnable, ActionListener{
	
	String[] tituloPagina = new String[6];
	URL[] enlacePagina = new URL[6];
	Color darColor = new Color(255, 204, 158);
	int actual = 0;
	Thread ejecutor;
	
	public CarruselGui(){
		tituloPagina[0] = "Buscador";
		enlacePagina[0] = getURL("http://www.google.com");
		tituloPagina[1] = "Red Social";
		enlacePagina[1] = getURL("http://www.twitter.com");
		tituloPagina[2] = "Ciclismo";
		enlacePagina[2] = getURL("http://www.foromtb.com");
		tituloPagina[3] = "Noticias";
		enlacePagina[3] = getURL("http://www.eldiario.com");
		tituloPagina[4] = "Agregador";
		enlacePagina[4] = getURL("http://www.meneame.net");
		tituloPagina[5] = "Programación";
		enlacePagina[5] = getURL("http://www.javahispano.org");
		JButton botonIr = new JButton("Ir");
		botonIr.addActionListener(this);
		setLayout(new FlowLayout());
		add(botonIr);
		setSize(300, 400);
		setVisible(true);
	}
	
	private URL getURL(String direccion){
		URL paginaURL = null;
		
		try{
			paginaURL = new URL(direccion);
		} catch(MalformedURLException ex){
			
		}
		return paginaURL;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CarruselGui carrusel = new CarruselGui();
	}

}
