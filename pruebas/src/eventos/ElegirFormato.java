package eventos;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class ElegirFormato	extends JFrame
							implements ItemListener{

	String[] formatos = {"(Elige formato)", "VHS", "BETA", "MP3", "Cassette"};
	
	String[] descripciones = 	{
									"Dos ruedines",
									"Un ruedín",
									"Modernillo",
									"Nostálgico"
								};
	JComboBox cajaFormato = new JComboBox();
	JLabel etiquetaDescripcion = new JLabel();
	
	public ElegirFormato(){
		super("Fomatos de cosas");
		setSize(420, 125);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
		for (int i = 0; i < formatos.length; i++)
			cajaFormato.addItem(formatos[i]);
		cajaFormato.addItemListener(this);
		add(BorderLayout.NORTH, cajaFormato);
		add(BorderLayout.CENTER, etiquetaDescripcion);
		setVisible(true);
	}
	
	public void itemStateChanged(ItemEvent evento){
		int eleccion = cajaFormato.getSelectedIndex();
		etiquetaDescripcion.setText(descripciones[eleccion - 1]);
		repaint();
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ElegirFormato formatos = new ElegirFormato();
	}

}
