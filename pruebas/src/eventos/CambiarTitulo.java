package eventos;

import java.awt.event.*;
import javax.swing.*;
import java.awt.*;

public class CambiarTitulo 	extends JFrame
							implements ActionListener{

	
	JButton b1 = new JButton("Curso de JavaScript");
	JButton b2 = new JButton("Curso de PHP");

	public CambiarTitulo(){
		super("Barra de título");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		b1.addActionListener(this);
		b2.addActionListener(this);
		setLayout(new FlowLayout());
		add(b1);
		add(b2);
		pack();
		setVisible(true);
	}

	public void actionPerformed(ActionEvent evento){
		if (evento.getSource() == b1)
			setTitle(b1.getText());
		else if (evento.getSource() == b2)
			setTitle(b2.getText());
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		CambiarTitulo ct = new CambiarTitulo();
		
	}

}
