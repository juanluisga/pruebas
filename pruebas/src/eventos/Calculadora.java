package eventos;

import java.awt.event.*;
import javax.swing.*;
import java.awt.*;

public class Calculadora 	extends JFrame
							implements FocusListener{
	
	JTextField valor1 = new JTextField("0", 5);
	JLabel mas = new JLabel("+");
	JTextField valor2 = new JTextField("0", 5);
	JLabel igual = new JLabel("=");
	JTextField suma = new JTextField("0", 5);
	
	public Calculadora(){
		super("Añade dos números");
		setSize(350, 90);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(new FlowLayout());
		valor1.addFocusListener(this);
		valor2.addFocusListener(this);
		suma.setEditable(false);
		add(valor1);
		add(mas);
		add(valor2);
		add(igual);
		add(suma);
		pack();
		setVisible(true);
		
		
	}

	public void focusGained(FocusEvent evento){
		try {
			// convertir valores a números
			float total = Float.parseFloat(valor1.getText()) + Float.parseFloat(valor2.getText());
			suma.setText(String.valueOf(total));
			
		} catch(NumberFormatException nfe){
			valor1.setText("0");
			valor2.setText("0");
			suma.setText("0");
		}
	}
	
	public void focusLost(FocusEvent evento){
		focusGained(evento);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Calculadora calc = new Calculadora();
	}

}
