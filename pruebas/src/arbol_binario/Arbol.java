package arbol_binario;

public class Arbol {
    Nodo raiz=null;
    public boolean tieneRaiz(){
        if(raiz==null) return false;
        else return true;
    }
 
    public Arbol alta(int dat){
        if(!tieneRaiz()){
            Nodo nuevo=new Nodo(dat);
            raiz=nuevo;
        }else{
            boolean izq;
            Nodo actual=raiz;
            while(true){
                if(actual.dato<dat) izq=false;
                else izq=true;
                if(!izq){
                    if(actual.der==null){
                        Nodo nuevo=new Nodo(dat);
                        actual.der=nuevo;
                        break;
                    }else actual=actual.der;
                }else{
                    if(actual.izq==null){
                        Nodo nuevo=new Nodo(dat);
                        actual.izq=nuevo;
                        break;
                    }else actual=actual.izq;
                }
            }
        }
        return this;
    }
 
    public boolean baja(int dat){
        Nodo actual=raiz, anterior=raiz, temp;
        while(true){
            if(actual==null) break;
            if(actual.dato==dat) break;
            anterior=actual;
            if(actual.dato<dat) actual=actual.der;
            else actual=actual.der;
        }
        if(actual==null) return false;
        else{
            if(actual==raiz){
                temp=actual.izq;
                raiz=raiz.der;
                anterior=raiz;
            }else
            	if (anterior.der == actual){
            		temp=actual.izq;
            		anterior=actual.der;
            	}else{
            		temp=actual.izq;
            		anterior.der=actual.izq;
            	}
            	actual=new Nodo();
            	while(actual.izq!=null)
            		actual=actual.izq;
            	actual.izq=temp;
            	return true;
        }
    }
 
    public void imprimirpreorden(){
        ayudantePreorden(raiz);
    }
 
    public void ayudantePreorden(Nodo dat){
        if(dat==null) return;
        System.out.printf("%d ",dat.dato);
        ayudantePreorden(dat.der);
        ayudantePreorden(dat.izq);
    }
 
    public void impririnorden(Nodo dat){
        if(dat!=null){
        	impririnorden(dat.izq);
            System.out.println(" "+dat.dato);
            impririnorden(dat.der);
        }
    }
}
