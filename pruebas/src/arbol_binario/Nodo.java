package arbol_binario;

public class Nodo {
    int dato;
    Nodo der;
    Nodo izq;
    
    public Nodo(int dat){
        this.dato=dat;
        this.der=null;
        this.izq=null;
    }
    
    public Nodo(){
    	// Constructor vacío.
    }
}
