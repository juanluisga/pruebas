package streams;

import java.io.*;

public class TodoMayusculas {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Mayusculas may = new Mayusculas(args[0]);
		may.convertir();

	}

}

class Mayusculas{
	String nombreFuente;
	
	public Mayusculas(String fuente){
		this.nombreFuente = fuente;
	}
	
	public void convertir(){
		File fuente = null;
		File temp = null;
		FileReader fr = null;
		BufferedReader br = null;
		FileWriter fw = null;
		BufferedWriter bw = null;
		try{
			// creación objetos File
			fuente = new File(this.nombreFuente);
			temp = new File("may" + this.nombreFuente + ".tmp");
			
			// crear StreamInput
			fr = new FileReader(fuente);
			br = new BufferedReader(fr);
			
			// crear StreamOutput
			fw = new FileWriter(temp);
			bw = new BufferedWriter(fw);
			
			// operaciones lectura/escritura
			boolean eof = false;
			int inChar;
			while (!eof){
				inChar = br.read();
				if (inChar == -1) eof = true;
				else bw.write(Character.toUpperCase((char)inChar));
			}
			
		} catch (IOException ex){
			System.out.println(ex.getMessage());
		} finally{
			try{
				bw.close();
				fw.close();
				br.close();
				fr.close();
			} catch (IOException ex){
				System.out.println(ex.getMessage());
			}
		}
		
		if (fuente.delete()) temp.renameTo(fuente);
	}
}
