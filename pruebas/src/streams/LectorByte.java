package streams;

import java.io.*;

/*
 * Videotutoriales nº 29 
 * https://www.youtube.com/watch?v=5P525BRLH9A&index=29&list=PL4D956E5314B9C253
 */

public class LectorByte{

	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		FileInputStream archivo = null; 
		boolean eof = false;
		int contador = 0;
		int entrada;		
		try {
			System.out.println("Directorio: " + new File(".").getCanonicalPath());
			archivo = new FileInputStream("clases.dat");
			while (!eof){
				entrada = archivo.read();
				if (entrada != -1){
					System.out.print(entrada + " ");
					contador++;
				}else
					eof = true;
			}
		} catch (FileNotFoundException ex){
			System.out.println("Fichero no encontrado");
		} catch (IOException ex){
			System.out.println(ex.toString());
		}
		finally{
			archivo.close();
			System.out.println("Bytes leídos: " + contador);
		}
	}

}
