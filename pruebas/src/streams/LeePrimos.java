package streams;

import java.io.*;

public class LeePrimos {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			FileInputStream archivo = new FileInputStream("250primos.dat");
			BufferedInputStream buff = new BufferedInputStream(archivo);
			DataInputStream datos = new DataInputStream(buff);
			
			try{
				while (true){
					int in = datos.readInt();
					System.out.print(in + " ");
				}
			} catch(EOFException exEOF){
				datos.close();
				buff.close();
				archivo.close();
			}
		} catch (IOException ex){
			
		}
	}

}
