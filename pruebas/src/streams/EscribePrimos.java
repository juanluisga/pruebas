package streams;


import java.io.*;

public class EscribePrimos{
	
	public EscribePrimos(){
		int cantidad = 250;
		int[] primos = new int[cantidad]; 
		int numPrimos = 0;
		// candidato: el número puede ser primo.
		int candidato = 2;
		while (numPrimos < cantidad){
			if (esPrimo(candidato)){
				primos[numPrimos] = candidato;
				numPrimos++;
			}
			candidato++;
		}
		
		try{
			// Escribir salida a disco
			FileOutputStream archivo = new FileOutputStream("250primos.dat");
			BufferedOutputStream buff = new BufferedOutputStream(archivo);
			DataOutputStream datos = new DataOutputStream(buff);
			
			for (int i = 0; i < 250; i++)
				datos.writeInt(primos[i]);
			datos.close();
			
		} catch (IOException ex){
			System.out.println("Error: " + ex.getMessage());
		}
	}
	
	public static boolean esPrimo(int numero){
		double raiz = Math.sqrt(numero);
		for (int i = 2; i <= raiz; i++){
			if (numero % i == 0) return false;
		}
		return true;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EscribePrimos numeros = new EscribePrimos();
	}

}
