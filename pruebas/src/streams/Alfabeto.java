package streams;

import java.io.*;

public class Alfabeto {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try{
			FileWriter fw = new FileWriter("alfabeto.txt");
			for (byte i = 65; i < 91; i++) fw.write((char)i);
			fw.close();
		} catch (IOException ex){
			System.out.println(ex.getMessage());
		}
	}

}
