package streams;

import java.io.*;

public class EntradaConsola {
	public static String leerLinea(){
		StringBuffer respuesta = new StringBuffer();
		try{
			BufferedInputStream buff = new BufferedInputStream(System.in);
			int in = 0;
			char inChar;
			do {
				in = buff.read();
				inChar = (char)in;
				if ( (in != -1) && (inChar != '\n') && (inChar != '\r' ) )
					respuesta.append(inChar);
			} while ( (in != -1) && (inChar != '\n') && (inChar != '\r' ) );
			buff.close();
			return respuesta.toString();
		} catch (IOException ex){
			System.out.println("Error: " + ex.getMessage());
			return null;
		}
	}
	
	public static void main(String[] args){
		System.out.print("\n¿Cuál es tu nombre? ");
		String entrada = leerLinea();
		System.out.println("\nHola " + entrada);
	}
}
