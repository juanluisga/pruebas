package streams;

import java.io.*;

public class LeerFuente {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		FileReader fr = null;
		BufferedReader buffer = null;
		try{
			fr = new FileReader("LeerFuente.java");
			buffer = new BufferedReader(fr);
			boolean eof = false;
			String linea = null;
			while ( !eof ){
				linea = buffer.readLine();
				if (linea == null) eof = true;
				else System.out.println(linea);
			}
			
		} catch (IOException ex){
			System.out.println("Error " + ex.getMessage());
		} finally{
			try {
				if (buffer != null)	buffer.close();
				if (fr != null) fr.close();
			} catch (IOException ex){}
		}
	}

}
