package streams;

import java.io.*;

public class EscribirByte {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int[] datos = {12, 54, 78, 34, 22, 56, 90, 21, 37, 86, 51, 32, 60, 45, 34, 10, 46, 87};
		try{
			FileOutputStream archivo = new FileOutputStream("pic.gif");
			for (int i = 0; i < datos.length; i++)
				archivo.write(datos[i]);
			archivo.close();
		} catch(IOException ex){
			System.out.print(ex.toString());
		}
	}

}
