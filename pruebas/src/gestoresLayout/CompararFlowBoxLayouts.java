package gestoresLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class CompararFlowBoxLayouts extends JFrame{

	JButton boton1 = new JButton("1");
	JButton boton2 = new JButton("2");
	JButton boton3 = new JButton("3");
	JButton boton4 = new JButton("4");
	JButton boton5 = new JButton("5");
	JButton boton6 = new JButton("6");
	
	public CompararFlowBoxLayouts(){
		super("Comparar FlowLayout y BoxLayout");
		setSize(150,250);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		JPanel panelFlow = new JPanel(new FlowLayout());
		JPanel panelBox  = new JPanel();
		panelBox.setLayout(new BoxLayout(panelBox, BoxLayout.X_AXIS));
		
		
		panelFlow.add(boton1);
		panelFlow.add(boton2);
		panelFlow.add(boton3);
		panelBox.add(boton4);
		panelBox.add(boton5);
		panelBox.add(boton6);
		
		setLayout(new FlowLayout());
		
		add(panelFlow);
		add(panelBox);
		
		pack();
		setVisible(true);
		
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		CompararFlowBoxLayouts ejemplo = new CompararFlowBoxLayouts();

	}

}
