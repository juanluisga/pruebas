package gestoresLayout;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class AsistenteEncuesta 	extends JPanel
								implements ActionListener {

	int cartaActual = 0;
	CardLayout cartas = new CardLayout();
	PanelEncuesta[] pregunta = new PanelEncuesta[3];
	
	public AsistenteEncuesta(){
		super();
		setSize(240, 140);
		setLayout(cartas);
		
		String pregunta1 = "¿Cuál es tu género?";
		String[] respuesta1 = {"Hombre", "Mujer", "No contesto"};
		String pregunta2 = "Cuál es tu edad?";
		String[] respuesta2 = {"Menor de 25", "De 26 a 34", "De 34 a 54", "Mayor de 54"};
		String pregunta3 = "¿Con qué frecuencia haces ejercicio a la semana";
		String[] respuesta3 = {"Nunca", "1-3 veces", "Más de 3 veces"};
		pregunta[0] = new PanelEncuesta(pregunta1, respuesta1, 2);
		pregunta[1] = new PanelEncuesta(pregunta2, respuesta2, 1);
		pregunta[2] = new PanelEncuesta(pregunta3, respuesta3, 1);
		pregunta[2].setPreguntaFinal(true);
		for (int i = 0; i < pregunta.length; i++){
			pregunta[i].botonSiguiente.addActionListener(this);
			pregunta[i].botonFinal.addActionListener(this);
			add(pregunta[i], "Carta " + i);
		}
 	}
	
	public void actionPerformed(ActionEvent evt){
		cartaActual++;
		if (cartaActual >= pregunta.length){
			System.exit(0);
		}
		cartas.show(this, "Carta " + cartaActual);
	}

}

class PanelEncuesta extends JPanel{
	JLabel pregunta;
	JRadioButton[] respuesta;
	JButton botonSiguiente = new JButton("Siguiente");
	JButton botonFinal = new JButton("Final");
	
	public PanelEncuesta(String pre, String[] resp, int def){
		super();
		setSize(160, 110);
		pregunta = new JLabel(pre);
		respuesta = new JRadioButton[resp.length];
		ButtonGroup grupo = new ButtonGroup();
		JPanel sub1 = new JPanel();
		sub1.add(pregunta);
		JPanel sub2 = new JPanel();
		for (int i = 0; i < resp.length; i++){
			if (def == i)
				respuesta[i] = new JRadioButton(resp[i], true);
			else
				respuesta[i] = new JRadioButton(resp[i], false);
			grupo.add(respuesta[i]);
			sub2.add(respuesta[i]);
		}
		JPanel sub3 = new JPanel();
		botonSiguiente.setEnabled(true);
		botonFinal.setEnabled(false);
		sub3.add(botonSiguiente);
		sub3.add(botonFinal);
		
		GridLayout grid = new GridLayout(3, 1);
		setLayout(grid);
		add(sub1);
		add(sub2);
		add(sub3);
	}
	
	public void setPreguntaFinal(boolean preguntaFinal){
		if (preguntaFinal){
			botonSiguiente.setEnabled(false);
			botonFinal.setEnabled(true);
		} 
	}
}