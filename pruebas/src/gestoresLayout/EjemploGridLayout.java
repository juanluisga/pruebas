package gestoresLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class EjemploGridLayout extends JFrame{

	public EjemploGridLayout(){
		super("Ejemplo BoxLayout");
		setSize(300,250);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(new GridLayout(3,3,10,10));
		add(new Button("NO"));
		add(new Button("N"));
		add(new Button("NE"));
		add(new Button("O"));
		add(new Button("C"));
		add(new Button("E"));
		add(new Button("SO"));
		add(new Button("S"));
		add(new Button("SE"));
		pack();
		setVisible(true);
		
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		EjemploGridLayout ejemplo = new EjemploGridLayout();

	}

}
