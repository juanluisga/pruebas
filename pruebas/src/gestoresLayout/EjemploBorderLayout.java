package gestoresLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class EjemploBorderLayout extends JFrame{

	public EjemploBorderLayout(){
		super("Ejemplo BorderLayout");
		setSize(300,250);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
		add(new Button("NORTE"), BorderLayout.NORTH);
		add(new Button("SUR"), BorderLayout.SOUTH);
		add(new Button("ESTE"), BorderLayout.EAST);
		add(new Button("OESTE"), BorderLayout.WEST);
		add(new Button("CENTRO"), BorderLayout.CENTER);
		pack();
		setVisible(true);
		
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		EjemploBorderLayout ejemplo = new EjemploBorderLayout();

	}

}
