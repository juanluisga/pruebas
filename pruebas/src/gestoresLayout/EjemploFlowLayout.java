package gestoresLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class EjemploFlowLayout extends JFrame{

	JButton boton1 = new JButton("1");
	JButton boton2 = new JButton("2");
	JButton boton3 = new JButton("3");
	JButton boton4 = new JButton("4");
	JButton boton5 = new JButton("5");
	JButton boton6 = new JButton("6");
	
	public EjemploFlowLayout(){
		super("Ejemplo FlowLayout");
		setSize(300,120);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(new FlowLayout());
		add(boton1);
		add(boton2);
		add(boton3);
		add(boton4);
		add(boton5);
		add(boton6);
		pack();
		setVisible(true);
		
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		EjemploFlowLayout ejemplo = new EjemploFlowLayout();

	}

}
