package gestoresLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class EjemploBoxLayout extends JFrame{

	JButton boton1 = new JButton("1");
	JButton boton2 = new JButton("2");
	JButton boton3 = new JButton("3");
	JButton boton4 = new JButton("4");
	JButton boton5 = new JButton("5");
	JButton boton6 = new JButton("6");
	
	public EjemploBoxLayout(){
		super("Ejemplo BoxLayout");
		setSize(300,250);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(new BoxLayout(getContentPane(),BoxLayout.Y_AXIS));
		add(boton1);
		add(boton2);
		add(boton3);
		add(boton4);
		add(boton5);
		add(boton6);
		pack();
		setVisible(true);
		
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		EjemploBoxLayout ejemplo = new EjemploBoxLayout();

	}

}
