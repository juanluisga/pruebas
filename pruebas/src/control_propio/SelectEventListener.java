package control_propio;

import java.util.EventListener;

/*
 * Interface que deberan implementar todos aquellos objetos
 * que deseen utilizar los eventos de nuestro objeto.
 * Define todos aquellos eventos que pueden ser disparados.
 */
public interface SelectEventListener extends EventListener {
    public abstract void onSelect(SelectEvent ev);
}
