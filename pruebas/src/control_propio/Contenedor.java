package control_propio;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Contenedor 	extends JFrame
							implements 	SelectEventListener{
	
	MiControl miControl = new MiControl();
	
	public Contenedor(){
		super("Test MiControl");
		setSize(300,200);
		setLayout(new FlowLayout());
		miControl.addEventListener(this);
		add(miControl);
		setVisible(true);
	}
	
	@Override
	public void onSelect(SelectEvent evento){
		if (evento.getSource() == miControl){
			JOptionPane.showMessageDialog(null, "¡Botón de miControl pulsado!"); 
			
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Contenedor contenedor = new Contenedor();
	}

}
