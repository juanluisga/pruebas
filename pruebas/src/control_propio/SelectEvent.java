package control_propio;

import java.util.EventObject;

/*
 * Esta clase definirá el evento que queremos personalizar.
 * En este caso es un evento de selección.
 * Se podría obviar y utilizar directamente la clase de la que
 * hereda (EventObject), pero por motivos de claridad, es mejor
 * crearla.
 */
public class SelectEvent extends EventObject {
	
	MiControl miControl;
	
	public SelectEvent(Object source, MiControl miControl ){
		super(source);
		this.miControl = miControl; 
		
	}

}
