package control_propio;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.ListIterator;

public class MiControl 	extends JPanel 
						implements 	ActionListener{
	
	private static ArrayList listeners;
	
	private JTextField textField = new JTextField(8);
	private JButton button = new JButton("Aceptar");
	
	
	/*
	 * Proporcionamos un mecanismo de registrar listeners,
	 * de modo que cuando se dispare el evento, se recorre
	 * la lista de listeners invocando al método que deben 
	 * tener definido para gestionar dicho evento.
	 */
	private void triggerSelectEvent(){
        ListIterator li = listeners.listIterator();
        while (li.hasNext()) {
            SelectEventListener listener = (SelectEventListener) li.next();
            SelectEvent readerEvObj = new SelectEvent(this, this);
            (listener).onSelect(readerEvObj);
        }		
	}
	
	public MiControl(){
		super();
		listeners = new ArrayList();
		setLayout(new FlowLayout());
		button.addActionListener(this);
		add(textField);
		add(button);
		setVisible(true);
	}
	
	/*
	 * Método público que permite registrar listeners.
	 */
	public void addEventListener(SelectEventListener listener){
		listeners.add(listener);
	}
	
	public void actionPerformed(ActionEvent evento){
		if (evento.getSource() == button){
			if (!textField.getText().equals("")){
				triggerSelectEvent();
			}
		}
	}
	


}
