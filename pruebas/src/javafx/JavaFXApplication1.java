package javafx;

import javafx.application.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.*;

/*
 * Tomado del libro "Guía Práctica Java 8"
 * de Roberto Montero Miguel
 * Editorial ANAYA
 */


public class JavaFXApplication1 extends Application {

	@Override
	public void start(Stage primaryStage){
		Label label = new Label();
		//label.setFont(new Font("Verdana", 36));
		label.setRotate(90);
		label.setTranslateY(50);
		label.setTranslateX(-50);
		label.setText("Hola Mundo");
		
		StackPane root = new StackPane();
		root.getChildren().add(label);
		
		Scene scene = new Scene(root, 200, 200);
		
		primaryStage.setTitle("Primer ejemplo");
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(args);
		
	}

}
