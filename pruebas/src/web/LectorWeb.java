package web;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.net.*;
import java.io.*;


public class LectorWeb extends JFrame{
	
	JTextArea caja = new JTextArea("Obteniendo datos...");
	
	public LectorWeb(){
		super("Obtener Archivo Aplicación");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(600, 300);
		JScrollPane panel = new JScrollPane(caja);
		add(panel);
		setVisible(true);
	}
	
	private void obtenerDatos(String direccion) throws MalformedURLException{
		setTitle(direccion);
		URL pagina = new URL(direccion);
		StringBuffer texto = new StringBuffer();
		try{
			HttpURLConnection con = (HttpURLConnection) pagina.openConnection();
			con.connect();
			InputStreamReader isr = new InputStreamReader((InputStream)con.getContent());
			BufferedReader br = new BufferedReader(isr);
			caja.setText("Obteniendo datos...");
			String linea;
			do{
				linea = br.readLine();
				texto.append(linea + "\n");
			} while ( linea != null);
			caja.setText(texto.toString());
			
			
		} catch(IOException ex) {
			System.out.println(ex.getMessage());
		}
	}
	
	public static void main(String[] args){
		if (args.length <  1){
			System.out.println("Es necesario pasar como argumento una URL");
			System.exit(1);
		} 
		try {
			LectorWeb apli = new LectorWeb();
			apli.obtenerDatos(args[0]);
		} catch(MalformedURLException ex){
			System.out.println("Formato de URL incorrecto.");
		}
	}

}
